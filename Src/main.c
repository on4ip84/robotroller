/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "CAN_API.h"
#include "InitDrive.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
volatile uint16_t asSpiWordLow = 0;
volatile uint16_t asSpiWordHigh = 0;
volatile uint32_t asResultWord = 0;
typedef struct AS5045_data {

};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#define PWM_FRQ_HZ (20000)
#define ALG_TIMER_HZ (1000)
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	__disable_irq();
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
	SysTick_Config(0xFFFFFF);
	LL_DBGMCU_APB1_GRP1_FreezePeriph( LL_DBGMCU_APB1_GRP1_TIM3_STOP);
	LL_DBGMCU_APB1_GRP1_FreezePeriph( LL_DBGMCU_APB1_GRP1_TIM2_STOP);
	LL_DBGMCU_APB2_GRP1_FreezePeriph( LL_DBGMCU_APB2_GRP1_TIM1_STOP);
	LL_DBGMCU_APB2_GRP1_FreezePeriph( LL_DBGMCU_APB2_GRP1_TIM8_STOP);
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
	SystemCoreClockUpdate();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  MX_SPI2_Init();
  MX_CAN1_Init();
  MX_CAN2_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */

	LL_GPIO_ResetOutputPin(LED1_GPIO_Port, LED1_Pin);
	LL_GPIO_ResetOutputPin(LED2_GPIO_Port, LED2_Pin);

	LL_ADC_EnableIT_JEOS(ADC1);
	LL_ADC_Enable(ADC1);

	/*Used center alogned mode so set ARR to Half of Calculated period*/
	uint32_t TIM_ARR_value = (SystemCoreClock / PWM_FRQ_HZ) >> 1;
	LL_TIM_SetAutoReload(TIM1, TIM_ARR_value - 1);
	LL_TIM_OC_SetCompareCH4(TIM1, LL_TIM_GetAutoReload(TIM1) - 1);
	LL_TIM_CC_EnableChannel(TIM1, LL_TIM_CHANNEL_CH4);

	LL_TIM_EnableIT_CC4(TIM1);
	LL_TIM_EnableCounter(TIM1);
	/*TIM2 on Half clock bus so divide SystemClock by 2 */
	LL_TIM_SetAutoReload(TIM2, ((SystemCoreClock / ALG_TIMER_HZ) >> 1) - 1);
	LL_TIM_EnableIT_UPDATE(TIM2);
	LL_TIM_EnableCounter(TIM2);

	LL_GPIO_SetOutputPin(AS5045_CS_GPIO_Port, AS5045_CS_Pin);
	LL_SPI_SetBaudRatePrescaler(SPI2, LL_SPI_BAUDRATEPRESCALER_DIV64);
	LL_SPI_Enable(SPI2);
	/*Init Control structure*/
	float TSAMPLE_FAST = 1.f / PWM_FRQ_HZ;
	float TSAMPLE_SLOW = 1.f / ALG_TIMER_HZ;
	DriveInit(TSAMPLE_FAST, TSAMPLE_SLOW);

	//initCAN(CAN_BAUD_1000);
	__enable_irq();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		if (!LL_SPI_IsActiveFlag_OVR(SPI2)) {
			LL_GPIO_ResetOutputPin(AS5045_CS_GPIO_Port, AS5045_CS_Pin);
			while (!LL_SPI_IsActiveFlag_TXE(SPI2)) {
			};
			LL_SPI_TransmitData16(SPI2, 0xFFFF);
			while (!LL_SPI_IsActiveFlag_RXNE(SPI2)) {
			}
			uint16_t buffer = LL_SPI_ReceiveData16(SPI2);
			asSpiWordLow = (buffer >> 4) & 0xFFF;
			LL_GPIO_SetOutputPin(AS5045_CS_GPIO_Port, AS5045_CS_Pin);

		} else {
			LL_SPI_ClearFlag_OVR(SPI2);
		}
		CAN_TxHeaderTypeDef txheader;
		uint8_t txdata[8];
		txheader.DLC = 8;
		txheader.ExtId = 0x11111111;
		txheader.IDE = CAN_ID_STD;
		txheader.RTR = CAN_RTR_DATA;
		for (uint8_t i = 0; i < 8; i++) {
			txdata[i] = i;
		}
		static uint32_t txMailBox = 0;
		if (HAL_CAN_AddTxMessage(&hcan2, &txheader, txdata,
				(uint32_t*) &txMailBox) != HAL_OK) {
			HAL_CAN_AbortTxRequest(&hcan2,
					CAN_TX_MAILBOX0 | CAN_TX_MAILBOX1 | CAN_TX_MAILBOX2);
		}
		/*
		if (HAL_CAN_AddTxMessage(&hcan1, &txheader, txdata,
				(uint32_t*) &txMailBox) != HAL_OK) {
			HAL_CAN_AbortTxRequest(&hcan1,
					CAN_TX_MAILBOX0 | CAN_TX_MAILBOX1 | CAN_TX_MAILBOX2);
		}
		if (HAL_CAN_AddTxMessage(&hcan2, &txheader, txdata,
				(uint32_t*) &txMailBox) != HAL_OK) {
			HAL_CAN_AbortTxRequest(&hcan2,
					CAN_TX_MAILBOX0 | CAN_TX_MAILBOX1 | CAN_TX_MAILBOX2);
		}
	*/
		HAL_Delay(10);
		static uint16_t state =0;
		//state = HAL_CAN_GetState(&hcan2);
		//LL_GPIO_TogglePin(CAN2_RX_GPIO_Port, CAN2_RX_Pin);
		//LL_GPIO_TogglePin(CAN2_TX_GPIO_Port, CAN2_TX_Pin);

	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* TIM1_BRK_TIM9_IRQn interrupt configuration */
  NVIC_SetPriority(TIM1_BRK_TIM9_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),1, 0));
  NVIC_EnableIRQ(TIM1_BRK_TIM9_IRQn);
  /* TIM1_CC_IRQn interrupt configuration */
  NVIC_SetPriority(TIM1_CC_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),3, 0));
  NVIC_EnableIRQ(TIM1_CC_IRQn);
  /* TIM2_IRQn interrupt configuration */
  NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),4, 0));
  NVIC_EnableIRQ(TIM2_IRQn);
  /* ADC_IRQn interrupt configuration */
  NVIC_SetPriority(ADC_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),2, 0));
  NVIC_EnableIRQ(ADC_IRQn);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
