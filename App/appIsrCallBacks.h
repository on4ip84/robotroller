/*
 * appIsrCallBacks.h
 *
 *  Created on: 18 авг. 2023 г.
 *      Author: on4ip
 */

#ifndef APPISRCALLBACKS_H_
#define APPISRCALLBACKS_H_

void adcIsrCallBack(void);
void tim1msCallBack(void);
void timPwmCallBack(void);


#endif /* APPISRCALLBACKS_H_ */
