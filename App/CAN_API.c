/*
 * CanCallBacks.c
 *
 *  Created on: 23 авг. 2023 г.
 *      Author: on4ip
 */
#include "CAN_API.h"

void initCAN(enum CAN_API_BAUD baud) {


	CAN_FilterTypeDef sFilterConfig;
	hcan2.Instance = CAN2;
	hcan2.Init.Prescaler = 5;
	hcan2.Init.Mode = CAN_MODE_NORMAL;
	hcan2.Init.SyncJumpWidth = CAN_SJW_1TQ;
	hcan2.Init.TimeSeg1 = CAN_BS1_1TQ;
	hcan2.Init.TimeSeg2 = CAN_BS2_7TQ;
	hcan2.Init.TimeTriggeredMode = DISABLE;
	hcan2.Init.AutoBusOff = DISABLE;
	hcan2.Init.AutoWakeUp = DISABLE;
	hcan2.Init.AutoRetransmission = DISABLE;
	hcan2.Init.ReceiveFifoLocked = DISABLE;
	hcan2.Init.TransmitFifoPriority = DISABLE;

	switch(baud)
	{
	case CAN_BAUD_125:
		hcan2.Init.Prescaler = 20;
		hcan2.Init.TimeSeg1 = CAN_BS1_15TQ;
		hcan2.Init.TimeSeg2 = CAN_BS2_2TQ;
		break;
	case CAN_BAUD_250:
		hcan2.Init.Prescaler = 10;
		hcan2.Init.TimeSeg1 = CAN_BS1_15TQ;
		hcan2.Init.TimeSeg2 = CAN_BS2_2TQ;
		break;
	case CAN_BAUD_500:
		hcan2.Init.Prescaler = 10;
		hcan2.Init.TimeSeg1 = CAN_BS1_1TQ;
		hcan2.Init.TimeSeg2 = CAN_BS2_7TQ;
		break;
	case CAN_BAUD_1000:
		hcan2.Init.Prescaler = 5;
		hcan2.Init.TimeSeg1 = CAN_BS1_1TQ;
		hcan2.Init.TimeSeg2 = CAN_BS2_7TQ;
		break;
	default:
		hcan2.Init.Prescaler = 5;
		hcan2.Init.TimeSeg1 = CAN_BS1_1TQ;
		hcan2.Init.TimeSeg2 = CAN_BS2_7TQ;
		break;
	}

	if (HAL_CAN_Init(&hcan2) != HAL_OK) {
		Error_Handler();
	}

	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;
	if (HAL_CAN_ConfigFilter(&hcan2, &sFilterConfig) != HAL_OK) {
		/* Filter configuration Error */
		Error_Handler();
	}

	HAL_CAN_Start(&hcan2);

	/*##-4- Activate CAN RX notification #######################################*/
	if (HAL_CAN_ActivateNotification(&hcan2, CAN_IT_RX_FIFO0_MSG_PENDING)
			!= HAL_OK) {
		/* Notification Error */
		Error_Handler();
	}
}
CAN_RxHeaderTypeDef RxHeader1;
uint8_t RxData1[8];
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	  /* Get RX message */
	  if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &RxHeader1, RxData1) != HAL_OK)
	  {
	    /* Reception Error */
	    Error_Handler();
	  }

}
CAN_RxHeaderTypeDef RxHeader0;
uint8_t RxData0[8];
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	  /* Get RX message */
	  if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader0, RxData0) != HAL_OK)
	  {
	    /* Reception Error */
	    Error_Handler();
	  }
}
