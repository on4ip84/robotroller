/*
 * ControlFuncs.h
 *
 *  Created on: 20 авг. 2023 г.
 *      Author: on4ip
 */

#ifndef CONTROLFUNCS_H_
#define CONTROLFUNCS_H_

#include "main.h"
#include "SystemAPI.h"
#include "InitDrive.h"
#include "PositionReg.h"

void FAST_loop(SYSTEMobj_st *SYSTEMobj);
void SLOW_loop(SYSTEMobj_st *SYSTEMobj);
#endif /* CONTROLFUNCS_H_ */
