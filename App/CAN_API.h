/*
 * CAN_API.h
 *
 *  Created on: 23 авг. 2023 г.
 *      Author: on4ip
 */

#ifndef CAN_API_H_
#define CAN_API_H_
#include "main.h"
//#include "can.h"
enum CAN_API_BAUD
{
	CAN_BAUD_125,
	CAN_BAUD_250,
	CAN_BAUD_500,
	CAN_BAUD_1000,
	CAN_BUAD_END
};
void initCAN(enum CAN_API_BAUD baud );

#endif /* CAN_API_H_ */
