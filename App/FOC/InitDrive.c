/*
 * InitDrive.c
 *
 *  Created on: 9 июн. 2021 г.
 *      Author: a.ermakov
 */
#include "InitDrive.h"
/*Указание сопротивления мотора*/
#define RS 7.2f
/*Указание индуктивности мотора*/
#define LS 3.1e-3f
/*Указание полосы для регулятора в rad/sec*/
#define BAND 3250
SYSTEMobj_st SYSTEMobj_1={.DRIVE=&DRIVE,.IO=&IO,.OBSERVER_El=&Observer,.OBSERVER_M=&ObserverM,.PWM=&MY_PWM,.CurrentRegBand = BAND};
SYSTEMobj_st SYSTEMobj_2={.DRIVE=&DRIVE,.IO=&IO,.OBSERVER_El=&Observer,.OBSERVER_M=&ObserverM,.PWM=&MY_PWM};
void DriveInit(float Tsample_fast,float Tsample_slow)
{
	//y = 1,1287E-03x - 2,1845E+00

	IO_settings_ext.ADC_A_Ku = 1.1287E-03f;
	IO_settings_ext.ADC_A_shift = -2.1845E+00;
	IO_settings_ext.ADC_B_Ku = 1.1287E-03f;
	IO_settings_ext.ADC_B_shift = -2.1845E+00;
	IO_settings_ext.ADC_C_Ku = 1.1287E-03f;
	IO_settings_ext.ADC_C_shift = -2.1845E+00;
	IO_settings_ext.ADC_Udc_Ku = 0.009242578f;
	IO_settings_ext.ADC_Udc_shift = 0.0f;
	set_IOSettings(&IO,&IO_settings_ext);

  DRIVE.Tsample_fast = Tsample_fast;
  DRIVE.Tsample_slow = Tsample_slow;
  //DRIVE.Tsample_slow = Tsample_slow;
  DRIVE.motor_data.Rs = RS;
  DRIVE.motor_data.Lq = LS;
  DRIVE.motor_data.Ld = LS;
  DRIVE.motor_data.Emf = 9.f/330;
  set_PI_coefs(DRIVE.ppid_Id, DRIVE.motor_data.Ld*SYSTEMobj_1.CurrentRegBand, DRIVE.motor_data.Rs*SYSTEMobj_1.CurrentRegBand*DRIVE.Tsample_fast, 0.9f);
  set_PI_coefs(DRIVE.ppid_Iq, DRIVE.motor_data.Lq*SYSTEMobj_1.CurrentRegBand, DRIVE.motor_data.Rs*SYSTEMobj_1.CurrentRegBand*DRIVE.Tsample_fast, 0.9f);
  set_PI_coefs(DRIVE.ppid_Idc,0.5f, 0.0195f, 0.9f);
  set_PI_coefs(DRIVE.ppid_FW, 0, 0.016f, 0.9f);
  /*Настройка регулятора скорости*/
  set_PI_coefs(DRIVE.ppid_Speed, 0.06f, 0.0005f, 0.9f);
  /*установка параметров*/
  PARAMETERS.Torque_current_max = 0.75f;
  PARAMETERS.Torque_ramp_Up = 40000;
  PARAMETERS.Torque_ramp_Down = 40000;
  PARAMETERS.Brake_current_max = 0.75f;
  PARAMETERS.Brake_ramp_Up = 40000;
  PARAMETERS.Brake_ramp_Down = 40000;
  PARAMETERS.Ibattery_chargeMax  = 1.0f;
  PARAMETERS.Ibattery_dischargeMax = 1.0f;
  PARAMETERS.Idz_max = 0;
  PARAMETERS.Idz_min = -0;
  PARAMETERS.Max_speed		=300;
  PARAMETERS.Min_speed		=-300;
  PARAMETERS.Speed_ramp_Down = 200000;
  PARAMETERS.Speed_ramp_Up	= 200000;
  /*Установка опций*/
  actual_options.options.bit_FluxWeak1 = 1;
  actual_options.options.bit_FluxWeak2 = 0;
  actual_options.options.bit_Decouple = 0;
  DRIVE.options.options_word = actual_options.options_word;
  /*Filter vars init*/
  filters_init(1/25.f,1/5.f);
  initFilter(&GyroLowPass,DRIVE.Tsample_slow,1/100.f);
  /*Инициалиазция ограничений*/
  ///Инициализация абсолютных значений
  RESTRICTS.AbsoluteLimits[LowVolt_constraint].ValueMaximum = 100;
  RESTRICTS.AbsoluteLimits[LowVolt_constraint].ValueMinimum = 7;
  RESTRICTS.AbsoluteLimits[LowVolt_constraint].SpanMaximum = 6;
  RESTRICTS.AbsoluteLimits[LowVolt_constraint].SpanMinumum = 6;
  RESTRICTS.AbsoluteLimits[HighVolt_constraint].ValueMaximum = 100;
  RESTRICTS.AbsoluteLimits[HighVolt_constraint].ValueMinimum = 2;
  RESTRICTS.AbsoluteLimits[HighVolt_constraint].SpanMaximum = 3;
  RESTRICTS.AbsoluteLimits[HighVolt_constraint].SpanMinumum = 0;
  RESTRICTS.AbsoluteLimits[PositiveSpeed_constraint].ValueMaximum = 9000;
  RESTRICTS.AbsoluteLimits[PositiveSpeed_constraint].ValueMinimum = 0;
  RESTRICTS.AbsoluteLimits[PositiveSpeed_constraint].SpanMaximum = 1000;
  RESTRICTS.AbsoluteLimits[PositiveSpeed_constraint].SpanMinumum = 0;
  RESTRICTS.AbsoluteLimits[NegativeSpeed_constraint].ValueMaximum = 0;
  RESTRICTS.AbsoluteLimits[NegativeSpeed_constraint].ValueMinimum = -9000;
  RESTRICTS.AbsoluteLimits[NegativeSpeed_constraint].SpanMaximum = 1000;
  RESTRICTS.AbsoluteLimits[NegativeSpeed_constraint].SpanMinumum = 0;
  RESTRICTS.AbsoluteLimits[HighTemperature1_constraint].ValueMaximum = 80;
  RESTRICTS.AbsoluteLimits[HighTemperature1_constraint].ValueMinimum = 0;
  RESTRICTS.AbsoluteLimits[HighTemperature1_constraint].SpanMaximum = 5;
  RESTRICTS.AbsoluteLimits[HighTemperature1_constraint].SpanMinumum = 0;
  RESTRICTS.AbsoluteLimits[LowTemparature1_constraint].ValueMaximum = 0;
  RESTRICTS.AbsoluteLimits[LowTemparature1_constraint].ValueMinimum = -20;
  RESTRICTS.AbsoluteLimits[LowTemparature1_constraint].SpanMaximum = 5;
  RESTRICTS.AbsoluteLimits[LowTemparature1_constraint].SpanMinumum =0;
  ///инициализация настраиваемых значений
  RESTRICTS.ActualLimits[LowVolt_constraint].Span = 3;
  RESTRICTS.ActualLimits[LowVolt_constraint].Value = 9;
  RESTRICTS.ActualLimits[HighVolt_constraint].Span = 1;
  RESTRICTS.ActualLimits[HighVolt_constraint].Value = 65;
  RESTRICTS.ActualLimits[NegativeSpeed_constraint].Span  = 50;
  RESTRICTS.ActualLimits[NegativeSpeed_constraint].Value = -200 + RESTRICTS.ActualLimits[NegativeSpeed_constraint].Span;
  RESTRICTS.ActualLimits[PositiveSpeed_constraint].Span = 50;
  RESTRICTS.ActualLimits[PositiveSpeed_constraint].Value = 200  - RESTRICTS.ActualLimits[PositiveSpeed_constraint].Span;
  RESTRICTS.ActualLimits[HighTemperature1_constraint].Value = 75;
  RESTRICTS.ActualLimits[HighTemperature1_constraint].Span = 5;
  RESTRICTS.ActualLimits[LowTemparature1_constraint].Value = -20;
  RESTRICTS.ActualLimits[LowTemparature1_constraint].Span = 5;
  InitRestricts(&RESTRICTS);
  /*Для работы по механике */
  ObserverM.Tsample = Tsample_fast;
  ObserverM.Teta_diskr = toPI/4000;
  ObserverM.L0 = 0.96f;
  init_observer(&ObserverM);
  /*Для работы по электрике*/
  Observer.Tsample = Tsample_fast;
  Observer.Teta_diskr = toPI/571.f;
  Observer.L0 = 0.986f;
  init_observer(&Observer);
  /*Инициализация регулятора положения*/
  Position_reg_init();
}
