/*
 * Filters.c
 *
 *  Created on: 16 авг. 2021 г.
 *      Author: v.timokhin
 */

#include "Filters.h"

lowPass_st GyroLowPass = {0};

void initFilter(lowPass_st *filter,float Tsample,float Tp)
{
	filter->k1 = platform_exp(-Tsample/Tp);
	filter->k2 = 1.f-filter->k1;
	filter->filtVal = 0;
}

void filterProc(lowPass_st* filter,float rawVal)
{
	filter->filtVal = filter->k1 * filter->filtVal+filter->k2 * rawVal;
}
