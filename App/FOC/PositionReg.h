/*
 * PositionReg.h
 *
 *  Created on: 9 июн. 2021 г.
 *      Author: a.ermakov
 */

#ifndef APPLICATION_USER_POSITIONREG_H_
#define APPLICATION_USER_POSITIONREG_H_
#include "PlatformMath.h"
#include "pid_reg.h"
enum REG_status{
	DISABLED = 0,
	ENABLED,
	WORK,
};
extern PIDREG3 Pid_Position;
/*Rall halper function*/
static inline float rollPI(const float position_error){
	if(position_error>=PI)
	{
		return (position_error-toPI);
	}
	if(position_error<=-PI)
	{
		return (position_error+toPI);
	}
	return position_error;
}
static inline float roll2PI(const float position_error){
	if(position_error>=toPI)
	{
		return (position_error-toPI);
	}
	if(position_error<0)
	{
		return (position_error+toPI);
	}
	return position_error;
}
static float limitVal(float const ref,float max,float min)
{
	if(ref > max) return max;
	if(ref < min) return min;
	return ref;
}
void Position_reg_init(void);
float get_reference(void);
enum REG_status get_pos_reg_state(void);
void set_pos_reg_state(enum REG_status state);
void Position_reg_process(float ref,float fdb);
void Position_reg_processV2(float ref,float fdb,float speed_fdb);
void Speed_reg_process(float ref , float fdb);
void SetSpeedReg_IC(float Ui);
#endif /* APPLICATION_USER_POSITIONREG_H_ */
