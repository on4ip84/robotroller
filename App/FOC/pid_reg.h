/*
 * pid_reg.h
 *
 *  Created on: 4 июн. 2021 г.
 *      Author: a.ermakov
 */

#ifndef APPLICATION_USER_PID_REG_H_
#define APPLICATION_USER_PID_REG_H_
typedef struct {  float  Ref;   		// Input: Reference input
				  float  Fdb;   		// Input: Feedback input
				  float  Err;			// Variable: Error
				  float  Kp;			// Parameter: Proportional gain
				  float  Up;			// Variable: Proportional output
				  float  Ui;			// Variable: Integral output
				  float  Ud;			// Variable: Derivative output
				  float  Tfilt_d;		// Filter time constant for D in 1\Tf
				  float  Ui_max; //Max integral out
				  float  Ui_min; //Min int out
				  float  OutPreSat;	// Variable: Pre-saturated output
				  float  OutMax;		// Parameter: Maximum output
				  float  OutMin;		// Parameter: Minimum output
				  float  Out;   		// Output: PID output
				  float  SatErr;		// Variable: Saturated difference
				  float  Ki;			// Parameter: Integral gain
				  float  Kc;			// Parameter: Integral correction gain
				  float  Kd; 			// Parameter: Derivative gain
				  float  Up1;			// History: Previous proportional output
				  unsigned int dft_st_U;// default bit for pid out
		 	 	  void  (*calc)();	  	// Pointer to calculation function
				 } PIDREG3;

typedef PIDREG3 *PIDREG3_handle;

/*-----------------------------------------------------------------------------
Default initalizer for the PIDREG3 object.
-----------------------------------------------------------------------------*/
#define PIDREG3_DEFAULTS { 0, \
                           0, \
                           0, \
                          0.05, \
                           0, \
						   0,\
                           0, \
                           0,\
                           0,\
                           0, \
                           0, \
                           0.83, \
                           0, \
                           0, \
                           0, \
                            0.1, \
                           0.7, \
                           0, \
                           0, \
						   0,\
              			  (void (*)(unsigned int))pid_reg3_calc }

#define PIDREG3_DEFAULTS_CLAMP { 0, \
                           0, \
                           0, \
                          0.05, \
                           0, \
						   0,\
                           0, \
                           0,\
                           0,\
                           0, \
                           0, \
                           0.83, \
                           0, \
                           0, \
                           0, \
                            0.1, \
                           0.9, \
                           0, \
                           0, \
						   0,\
              			  (void (*)(unsigned int))pid_reg3_calc_clamp }

/*------------------------------------------------------------------------------
Prototypes for the functions in PIDREG3.C
------------------------------------------------------------------------------*/
void pid_reg3_calc(PIDREG3_handle);
void pid_reg3_calc_position(PIDREG3_handle);
void pid_reg3_calc_clamp(PIDREG3_handle);
void pid_reg3_calc_ErrLim(PIDREG3_handle);
void set_PI_coefs(PIDREG3_handle ,float Kp,float ,float );

#endif /* APPLICATION_USER_PID_REG_H_ */
