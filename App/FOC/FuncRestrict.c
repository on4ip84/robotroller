#include "PlatformMath.h"
#include "FOC_Control.h"
#include "FuncRestrict.h"
//-------------------------------------------------------
uint16_t InitRestricts(FuncRestricts_st *restricts)
{
	uint16_t OgrName=0;
	uint16_t Error = 0;
	for(OgrName=0;OgrName<Constraints_count;OgrName++)
	{
				// �������� ���������� ����������
				if( restricts->ActualLimits[OgrName].Value > restricts->AbsoluteLimits[OgrName].ValueMaximum )
				{	restricts->ActualLimits[OgrName].Value = restricts->AbsoluteLimits[OgrName].ValueMaximum ;
					Error |= ogrErrLimMax;
				}
				else if(restricts->ActualLimits[OgrName].Value < restricts->AbsoluteLimits[OgrName].ValueMinimum)
				{		restricts->ActualLimits[OgrName].Value = restricts->AbsoluteLimits[OgrName].ValueMinimum;
					Error |= ogrErrLimMin;
				};
				if( restricts->ActualLimits[OgrName].Span > restricts->AbsoluteLimits[OgrName].SpanMaximum )
				{	restricts->ActualLimits[OgrName].Span = restricts->AbsoluteLimits[OgrName].SpanMaximum;
					Error |= ogrErrDiapMax;
				}
				else if(restricts->ActualLimits[OgrName].Span < restricts->AbsoluteLimits[OgrName].SpanMinumum)
				{		restricts->ActualLimits[OgrName].Span = restricts->AbsoluteLimits[OgrName].SpanMinumum;
					Error |= ogrErrDiapMin;
				}
				restricts->Koef[OgrName] = 1.0f/restricts->ActualLimits[OgrName].Span;
	}
	return Error;
}

float ProcessRestricts(float value, FuncRestricts_st *restricts,enum Constraint_en OgrName)
{
	float temp = 1;
	switch(OgrName)
	{
		/*����������� �������� ���� �������� Val > Limit*/
		case HighVolt_constraint:
		case PositiveSpeed_constraint:
		case HighTemperature1_constraint:
		temp = 1.0f - restricts->Koef[OgrName]*(value-restricts->ActualLimits[OgrName].Value);
		break;
		/*����������� �������� ���� ������� Val < Limit*/
		case LowVolt_constraint:
		case NegativeSpeed_constraint:
		case LowTemparature1_constraint:
		temp = 1.0f + restricts->Koef[OgrName]*(value-restricts->ActualLimits[OgrName].Value);
		break;
		default:
		restricts->ContraintActive.ConstraintActive.Error = ogrErrProgErr;
	}
	if (temp > 1.0f){
						restricts->ContraintActive.ConstratinActive_word&=~(1<<OgrName);
						return 1.0f;
	}
	else if (temp < 0.f) 	{	restricts->ContraintActive.ConstratinActive_word |= 1<<OgrName; return 0.f;}
		else	{	restricts->ContraintActive.ConstratinActive_word  |= 1<<OgrName; return temp; }
	return temp;

}
/*!
 *
 * @param I_reference
 * @param W_real
 * @param I_trig
 * @return I_control
 */
float Low_speed_Current_derating(float I_reference, float W_real, float I_trig)
{
			//Åñëè òîê çàäàíèÿ áîëüøå íåêîé óñòàâêè, ïðåäïîëîæèòåëüíî  30% ìàêñèìàëüíîãî òîêà èíâåðòîðà
			//òî íà÷íåì îòñò÷åò âðåìåíè äëÿ ñíèæåíèÿ òîêà.
			static float Time_to_derate = 0;
			float I_control=1;
			//Åñëè ñêîðîñòü äâèãàòåëÿ ìåíüøå ïîðîãà òî îáñëóæèâàåì îãðàíè÷íèå
			if(W_real<200)
			{
				if(I_reference>=(I_trig*1))
				{
					Time_to_derate+=1e-3;//
					//Íà÷íåì îãðàíè÷åíèå åñëè òîê áîëüøå óñòàâêè â òå÷åíèè 5 ñåêóíä.
					if(Time_to_derate >=5)
					{
						I_control = (7-Time_to_derate)*0.5;//Îãðàíè÷íèå  ñïîëçåò äî íóëÿ çà 2 ñåêóíäû
						if(I_control<=0) I_control = 0;

					}//end of if
				}

			}else{

				if(I_reference<=(I_trig*0.95))
				{
					Time_to_derate-=0.1e-3;
					I_control = (7-Time_to_derate)*0.25;//Îãðàíè÷íèå  ñïîëçåò äî íóëÿ çà 2 ñåêóíäû
					if(I_control>=1) I_control = 1;

				}
			}//end of else
			if(Time_to_derate <=0) Time_to_derate = 0; //÷òîáû âðåìÿ íåáûëî îòðèöàòåëüíûì
			if(Time_to_derate >=7) Time_to_derate = 7;

			float res_current = (I_control*(I_reference-I_trig)+I_trig);
			if(I_control >=1)  return (I_reference);
			if(I_control <=0)  return (res_current);
			if(res_current < I_trig) return (I_trig);
			else return (res_current);
}


