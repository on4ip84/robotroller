/*
 * IOdata.c
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: a.ermakov
 */
#include  "IOdata.h"


IO_settings_st IO_settings_ext={0};
IO_settings_st IO_settings_int={0};
IO_st IO={.IO_coefs=&IO_settings_int};
void set_IOSettings(IO_st *io,IO_settings_st* settings)
{
	io->IO_coefs->ADC_A_Ku = settings->ADC_A_Ku;
	io->IO_coefs->ADC_A_shift = settings->ADC_A_shift;
	io->IO_coefs->ADC_B_Ku = settings->ADC_B_Ku;
	io->IO_coefs->ADC_B_shift = settings->ADC_B_shift;
	io->IO_coefs->ADC_C_Ku = settings->ADC_C_Ku;
	io->IO_coefs->ADC_C_shift = settings->ADC_C_shift;
	io->IO_coefs->ADC_Udc_Ku = settings->ADC_Udc_Ku;
	io->IO_coefs->ADC_Udc_shift = settings->ADC_Udc_shift;
}
static void convert_IOdata(IO_st* io,const uint16_t mode)
{
	io->current_A = io->ADC_A*io->IO_coefs->ADC_A_Ku + io->IO_coefs->ADC_A_shift;
	io->current_B = io->ADC_B*io->IO_coefs->ADC_B_Ku + io->IO_coefs->ADC_B_shift;
	if(io->ADC_C)
	{
		io->current_C = io->ADC_C*io->IO_coefs->ADC_C_Ku + io->IO_coefs->ADC_C_shift;
	}else io->current_C = -io->current_B-io->current_A;
	io->voltage_DC = io->ADC_Udc*io->IO_coefs->ADC_Udc_Ku + io->IO_coefs->ADC_Udc_shift;

}

void update_IO_ADC(IO_st* io,uint16_t *ADC_data,const uint16_t mode)
{
	io->ADC_A = (ADC_data[0] + ADC_data[1]) >> 1 ;
	io->ADC_B = (ADC_data[2] + ADC_data[3]) >> 1 ;
	io->ADC_C = 0;
	io->ADC_Udc = ADC_data[5];
	io->ADC_VrefInt = ADC_data[4];
	convert_IOdata(io,mode);

}
void update_IO_Sensor(IO_st* io,uint16_t pos_cnt)
{
	io->ENC_CNT_AB = pos_cnt;
}
