/*
 * FOC_Control.h
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: a.ermakov
 */

#ifndef APPLICATION_USER_FOC_CONTROL_H_
#define APPLICATION_USER_FOC_CONTROL_H_
#include <stdint.h>
#define FiledWeaking_coeff			0.75f
#define VoltageLimitation_coeff		0.866f
#define VoltageNormalisation_coeff	0.866f
/*structs*/
enum ControlMode
{
  TORQUE = 0,
  SPEED,
  REGEN_BRAKING,
  SWITCH_BRAKING,
  PARK_BRAKING,
  POSTION,
  DCREG
};
enum AlgMode
{
  EMPTY=0,
  FOC,
  SENSORSLESS,
  BLDC,
  SINE,
  ACIM
};
enum DetectMode
{
  STOP_DETECT=0,
  motorRS,
  motorLS,
  HALL_STEP,
  HALL_ROTARY,
  EMF
};
/*!
 * \brief Stucture for advanced options of control
 */
typedef struct
{
	uint16_t bit_ALG_MODE:3;		/*COntrol algorithm mode */
	uint16_t bit_FluxWeak1:1;	/*Flux weaking mode with voltage lim reg*/
	uint16_t bit_FluxWeak2:1;	/*Flux weaking mode with voltage angle reg*/
	uint16_t bit_Decouple:1;	/*Decoupling mode */
	uint16_t bit_PWM_OneLeg:1;	/*Discontinuous PWM mode*/
}options_st;
union options_un
{
	options_st options;
	uint16_t   options_word;
};
extern union options_un actual_options;
/*!
 *
 * \brief Stucture with COmmand bit defination
 */
typedef struct
{
  uint16_t bit_ON:1; 			/*Modulation ON command bit*/
  uint16_t bit_RST:1;			/*Reset Bit*/
  uint16_t bit_BRAKE:1;			/*Braking ON command bit*/
  uint16_t bit_CTRL_MODE:3;		/*Control mode*/
  uint16_t bit_DETECT_MODE:5;		/*Detect mode*/
  uint16_t bit_REF_INVERSE:1;		/*Reference inversion mode command bit*/
  uint16_t bit_PHASE_INVERS:1;		/*Phase inversion mode command bit*/
  uint16_t bit_SPEED_MODE:1;			/*Sine control mode command bit*/
  //uint16_t bit_FOC:1;			/*FOC control mode command bit*/
  //uint16_t bit_SENSORLESS:1;		/*SensorlEss mode command bit*/
  uint16_t bit_ALG_MODE:3;		/*COntrol algorithm mode */
}command_st;
enum DRIVE_MODE{
  MOTOR=0,
  GENERATOR
};
/*!
 *
 * \brief Stucture with Status bit defination
 */
typedef struct
{
  uint16_t bit_READY1:1;		/*1st ready bit NO error*/
  uint16_t bit_READY2:1;		/*2nd rady bit NO saturation*/
  uint16_t bit_ENABLED:1;		/*Modulation enabled Bit*/
  uint16_t bit_FAULT:1;			/*Global fault bit*/
  uint16_t bit_DRIVE_MODE:2;		/*Work in motor or gen mode bit*/
  uint16_t bit_IDENT_MODE:1;		/*Identification but*/
  uint16_t bit_SPEED_MODE:1;		/*Work in SPeed reg mode*/
  uint16_t bit_REVERSE_MODE:1;		/*WOrk in reverse*/
  uint16_t bit_FW_MODE:1;		/*Work in Fieldweaking*/
  uint16_t bit_SAT_MODE:1;		/*WOkr in saturation*/
  uint16_t bit_PROFILE:2;		/*Active profile*/
  uint16_t bit_HALL_DISABLE:1;	/*Hall disabledBit*/
}status_st;
/*!
 *  \brief Structure for Error Bits definitions
 */
typedef struct
{
	uint16_t bit_FREE1:1;			/*Empty bit*/
	uint16_t bit_FREE2:1;			/*Empty bit*/
	uint16_t bit_AmpCurr_err:1;		/*Amplitude current error bit*/
	uint16_t bit_Hpower:1;			/*Hardware error bit*/
	uint16_t bit_Hcurr_err:1;		/*Hardware ivercurrent bit*/
	uint16_t bit_Hvolt_err:1;		/*Hardware overvoltage error bit*/
	uint16_t bit_GasFault:1;		/*Throttle input error bit*/
	uint16_t bit_FREE3:1;			/*Empty bit*/
	uint16_t bit_OverVolt:1;		/*Software overvoltage bit*/
	uint16_t bit_OverCurr:1;		/*Software overcurrent error error bit*/
	uint16_t bit_OverTempBoard:1;	/*Software over tempr PowerBoard error bit*/
	uint16_t bit_OverTempMotor:1;	/*Software over tempr Motor error bit*/
	uint16_t bit_OverTempCntr:1;	/*Software over tempr COntrol Board error bit*/
	uint16_t bit_ISRoverTime:1;		/*main ISR over load error bit*/
	uint16_t bit_PosSens:1;			/*Position sensor error bit*/
}errors_st;
/*!
 *
 * \brief Structure with limitation parameters used in algorithm
 */
typedef struct
{
    float Torque_current_max;				///< Óñòàâêà îãðàíè÷åíèÿ çàäíèÿ òîêà
    float Brake_current_max;	///< Óñòàâêà ìàêñèìàëüíîãî òîêà òîðìîæåíèÿ
    float Current_Attention;			///< Óñòàâêà äëÿ àâàðèè ïî òîêó Ñðåäíåå çíà÷åíèå òîêà
    float Current_Error;				///< Óñòàâêà äëÿ àâàðèè ïî òîêó ÀÌÏËÈÒÓÄÀ ôàçíîãî òîêà
    float Udc_max;				///< Óñòàâêà äëÿ àâàðèè ïî ìàêñèìàëüíîìó íàïðÿæåíèþ Udc
    float Udc_min;				///< Óñòàâêà äëÿ àâàðèè ïî ìèíèìàëüíîìó íàïðÿæåíèþ Udc
    float Wmax_el_err;			///< Óñòàâêà äëÿ àâàðèè ïî ïðåâûøåíèþ ñêîðîñòè
    float Tmin_Board;			///< Ìèíèìàëüíàÿ òåìïåðàòóðà ñèëîàîé ïëàòû
    float Tmin_Cpu;				///< Ìèíèìàëüíàÿ òåìïåðàòóðà ïðîöåññîðíîé ïëàòû
    float Tmin_Wind;			///< Ìèíèìàëüíàÿ òåìïåðàòóðà Îáìîòêè ìîòîðà
    float Tmax_Board;			///< Ìàêñèìàëüíàÿ òåìïåðàòóðà ñèëîâîé ïëàòû
    float Tmax_Cpu;				///< Ìàêñèìàëüíàÿ òåìïåðàòóðà ïðîöåññîðíîé ïëàòû
    float Tmax_Wind;			///< Ìàêñèìàëüíàÿ òåìïåðàòóðà îáìîòêè
    float Mz_ogr;  				///< Óñòàâêà  îãðàíè÷åíèÿ çàäàíèÿ ìîìåíòà
    float Uz_ogr;  				///< Óñòàâêà  îãðàíè÷åíèÿ çàäàíèÿ íàïðÿæåíèÿ
    float P_ogr;   				///< Óñòàâêà  îãðàíè÷íèåÿ ìîùíîñòè
    float M_TADgenOgr;			///< Óñòàâêà  îãðàíè÷åíèå ãåíåðàòîðíîãî ìîìåíòà ( èñïîëüçóåòñÿ â ôóíêöèîíàëüíûõ îðàíè÷åíèÿõ)
    float P_TADgenOgr;			///< Óñòàâêà  îãðàíè÷åíèå ãåíåðàòîðíîé ìîùíîñòè ( èñïîëüçóåòñÿ â ôóíêöèîíàëüíûõ îðàíè÷åíèÿõ)
    float Ibattery_dischargeMax;    ///< Óñòàâêà ìàêñèìàëüíîãî òîêà ðàçðÿäà áàòàðåè
    float Ibattery_chargeMax;       ///< Óñòàâêà ìàêñèìàëüíî òîêà çàðÿäà áàòàðåè
    float Idz_min;				///< Óñòàâêà  îãðàíè÷åíèå òîêà íàìàãíè÷èâàíèÿ ñíèçó ïðè ðàáîòå ïðèâîäà..
    float Idz_max;				///< Óñòàâêà  îãðàíè÷åíèå òîêà íàìàãíè÷èâàíèÿ ñâåðõó ïðè ðàáîòå ïðèâîäà.
    float Max_ref_position;		///< Óñòàâêà  îãðàíè÷åíèÿ çàäàíèÿ óãëà äëÿ ðåãóëÿòîðà ïîëîæåíèÿ ñâåðõó
    float Min_ref_position;		///< Óñòàâêà  îãðàíè÷åíèÿ çàäàíèÿ óãëà äëÿ ðåãóëÿòîðà ïîëîæåíèÿ ñíèçó
    float Max_speed;			///< Óñòàâêà  îãðàíè÷åíèÿ ìàêñèìàëüíîé ñêîðîñòè ïî âûõîäó ðåãóîÿòîðà ïîëîæåíèÿ
    float Min_speed;			///< Óñòàâêà  îãðàíè÷åíèÿ ìàêñèìàëüíîé ñêîðîñòè ïî âûõîäó ðåãóîÿòîðà ïîëîæåíèÿ
    float Speed_ramp_Up;		///< Ðàìïà íàðàñòàíèÿ çàäàíèÿ ñêîðîñòè rad/s
    float Speed_ramp_Down;		///< Ðàìïà ñïàäà  çàäàíèÿ ñêîðîñòè rad/s0
    float Torque_ramp_Up;		///< Ðàìïà íàðàñòàíèÿ çàäàíèÿ ìîìîåíòà ( òîêà) A/s
    float Torque_ramp_Down;		///< Ðàìïà ñïàäà çàäàíèÿ ìîìåíòà (òîêà) A/s
    float Brake_ramp_Up;		///< Ðàìïà íàðàñòàíèÿ çàäàíèÿ òîðìîçíîãî ìîìîåíòà ( òîêà) A/s
    float Brake_ramp_Down;		///< Ðàìïà ñïàäà çàäàíèÿ òîðìîçíîãî ìîìåíòà (òîêà) A/s
} PARAM_st;
/*!
 *  \brief Structure for Drive definition
 */
typedef struct{
	uint16_t drive_ID;
	union{
		command_st cmd;							/*Command word Union*/
		uint16_t cmd_word;
		}CMD;
	union{
		errors_st errors;
		uint16_t errors_word;					/*Errors word union*/
		}ERRORS;
	union{
		status_st stat;
		uint16_t stat_word;						/*Status word union*/
		}STAT;
	struct currentABC_st{
	float current_Ia;
	float current_Ib;
	float current_Ic;
	}currentABC;
	struct currentClark_st{
	float current_Ialpha;
	float current_Ibeta;
	}currentClark;
	struct currentPark_st{
	float current_Iq;
	float current_Id;
	}currentPark;
	float torque_Mz;
	float torque_Mz_rated;
	float torque_Mz_limited;
	float torque_Mz_avaible;
	float speed_Ref;
	float speed_Fdb;
	float speed_Ref_rated;
	float current_Iq_ref;
	float current_Id_ref;
	float voltage_DC;
	float voltage_Vq_ref;
	float voltage_Vd_ref;
	float voltage_Vd_decoupl;
	float voltage_Vq_decoupl;
	float voltage_Valpha_ref;
	float voltage_Vbeta_ref;
	float voltage_Vref;
	float voltage_VrefFilt;
	float ElAngle;
	float ElSpeed;
	float ElSpeedFilt;
	float temperature_Winding;
	float temperature_Power;
	float temperature_Control;
	float power_Pdc;
	float current_Idc;
	float torque_calc;
	float flux_calc;
	float flux_shift_calc;
	float power_Mech;
	struct motor_st
	{
		float Rs;
		float Ld;
		float Lq;
		float Emf;
		float Kt;
		uint16_t P;
		float J;
		float B;
	}motor_data;
	float Tsample_fast;
	float Tsample_slow;
	PARAM_st *drive_parameters;
	union options_un options;
	void *ppid_Iq;
	void *ppid_Id;
	void *ppid_Idc;
	void *ppid_FW;
	void *ppid_Speed;
	void *prestricts;

}DRIVE_st;
extern DRIVE_st DRIVE;
extern PARAM_st PARAMETERS;
void filters_init(float, float);
void fast_loop_control(DRIVE_st* );
void slow_loop_control(DRIVE_st* );
#endif /* APPLICATION_USER_FOC_CONTROL_H_ */
