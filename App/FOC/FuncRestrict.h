/*
 * FuncRestrict.h
 *
 *  Created on: 13 ���. 2015 �.
 *      Author: Andrey Ermakov
 */

#ifndef BLDC_FOC_HALL_INCLUDE_FUNCRESTRICT_H_
#define BLDC_FOC_HALL_INCLUDE_FUNCRESTRICT_H_
//����������� ��� ���������� OgrManage.Err
#define ogrErrLimMin	0x0100	// ��������������. ������ ����������� ���� ������������ ������. ����������.
#define ogrErrLimMax	0x0200	// ��������������. ������ ����������� ���� ������������� ������. ����������.
#define ogrErrDiapMin	0x0400	// ��������������. ����� �������� ���� ���. ���������� ��������. ����������.
#define ogrErrDiapMax	0x0800	// ��������������. ����� �������� ���� ����. ����������� ��������. ����������.
#define ogrErrProgErr	0x0001	// ����������� ������ (�����������) ogrErrAbsWrong
#define ogrErrAbsWrong	0x0002	// �������� ������� ���������� ����������� ������������ ������� �� �� �������� ���

enum Constraint_en{
	HighVolt_constraint=0,
	LowVolt_constraint,
	PositiveSpeed_constraint,
	NegativeSpeed_constraint,
	HighTemperature1_constraint,
	HighTemperature2_constraint,
	HighTemperature3_constarint,
	LowTemparature1_constraint,
	LowTemparature2_constraint,
	LowTemparature3_constraint,
	Constraints_count
};

struct OgrManageStruct {
	unsigned int OgrStat;	// ��������� ����� ����������� ����������� (������ � ��������)
	unsigned int ZeroStat;	// ��������� ����� ������ ����������� (��������� �������)
	unsigned int Err;	// ��������� ����� ������ �������������. �������� ��. bitdefinition.h
};

struct OgrLimitsStruct {
	float	ValueMinimum;	// ����������� ����������� �������� �����������
	float	ValueMaximum;	// ������������ ����������� �������� �����������
	float	SpanMinumum;		// ����������� �����������  �������� ��������� ��������� �������
	float	SpanMaximum;		// ������������ ����������� �������� ��������� ��������� �������
};

struct OgrStruct
{	float 	Value;		// �������� �����������
	float 	Span;		// �������� �������� ���������� �� ������ ����������� �� ��������� �������
};
typedef struct
{
	union{
		uint16_t ConstratinActive_word;
		struct ContraintActive_st
		{
			uint16_t HighVolt_constraint:1;
			uint16_t LowVolt_constraint:1;
			uint16_t PositiveSpeed_constraint:1;
			uint16_t NegativeSpeed_constraint:1;
			uint16_t HighTemperature1_constraint:1;
			uint16_t HighTemperature2_constraint:1;
			uint16_t HighTemperature3_constarint:1;
			uint16_t LowTemparature1_constraint:1;
			uint16_t LowTemparature2_constraint:1;
			uint16_t LowTemparature3_constraint:1;
			uint16_t Error:6;
		}ConstraintActive;
	}ContraintActive;
	struct	OgrLimitsStruct	AbsoluteLimits[Constraints_count];
	struct	OgrStruct		ActualLimits[Constraints_count];
	float	Koef[Constraints_count];

}FuncRestricts_st;
extern FuncRestricts_st RESTRICTS;
//������������� ����������� (����������� ����������, �� ������������!)
struct TOgrType
{	float TwinOgr;	// ����������� �������, ��� ������� ���������� ����������� �������
	float Twin_Zero;	// ����������� �������, ��� ������� ������ ����������
	float Tboard;	// ����������� ����, ��� ������� ���������� ����������� �������
	float Tboard_Zero;// ����������� ����, ��� ������� ������ ����������
	float Tboard_min;	// ����������� ����������, ��� ������� ���������� ����������� �������
	float Tboard_min_Zero;// ����������� ����������, ��� ������� ������ ����������
};
uint16_t InitRestricts(FuncRestricts_st *restricts);
float ProcessRestricts(float value, FuncRestricts_st *restricts,enum Constraint_en OgrName);
float Low_speed_Current_derating(float I_reference, float W_real, float I_trig);

#endif /* BLDC_FOC_HALL_INCLUDE_FUNCRESTRICT_H_ */
