//Observer
#include "PlatformMath.h"
#include "observer.h"
Observer_struct Observer = {0};
Observer_struct ObserverM = {0};
float hall_corr[6]={1.2285765,1.82447,2.7726,3.80408,4.99,5.93};
void init_observer(Observer_struct *Observer)
{
 //Observer->Teta_diskr=1.047;
 Observer->POS_rad=0;
 Observer->last_CNT=0;
 Observer->POS_dobavka_sum=0;
 Observer->fullMode_ena=0;
 Observer->teta_correction=0;
 Observer->refresh_ena=1;
 refresh_koef(Observer);
 Observer->CNT=0;
 Observer->Time_CNT=0;
 Observer->W = 0;
 Observer->Teta_rad = 0;
 Observer->J = 0.00046;
 Observer->Ml = 0;
 Observer->Ml_izm=0;
}
FAST_RAM void speed_observer(Observer_struct *Observer);
void speed_observer(Observer_struct *Observer)
{
	
	

	 static float teta_dif=0;
	 static float imp_time=0;
	 static float w_last = 0;
	Observer->POS_rad=/*Observer->direction*/Observer->CNT *Observer->Teta_diskr;
	if(Observer->CNT!=Observer->last_CNT) // ������� ��� ���������� �������
 	{

 			Observer->POS_dobavka_sum = 0;//* 0.0f *sign(Observer->W);
 	}
    /* ����������� �� ������� ��� ������������ ����*/
    Observer->POS_rad+=Observer->teta_correction;
    if(Observer->POS_rad >=toPI) Observer->POS_rad-=toPI;
    if(Observer->POS_rad < 0) Observer->POS_rad+=toPI;

    Observer->last_CNT=Observer->CNT;

	//--------------------------------------------------------------------------
	//���������� ������ �� 2 ��
	
	teta_dif=(Observer->Teta_rad - Observer->POS_rad);
	if(teta_dif>PI)
	{
		teta_dif=teta_dif-toPI;
	}
	if(teta_dif<-PI)
	{
		teta_dif=teta_dif+toPI;
	}
	if(teta_dif>PI)
	{
		teta_dif=teta_dif-toPI;
	}
	if(teta_dif<-PI)
	{
		teta_dif=teta_dif+toPI;
	}


	//-----------------------------------------------------------
	// ��������������
	//------------------------------------------------------------
	if(!Observer->fullMode_ena){
		Observer->Teta_rad=Observer->Teta_rad+Observer->W*Observer->Tsample+Observer->L1_w*(teta_dif);//� ��������
		Observer->Teta_deg=Observer->Teta_rad*rad2Deg;
		Observer->W=Observer->W+Observer->L2_w*(teta_dif);//� �������� �������������
	}else{
		Observer->Teta_rad=Observer->Teta_rad + Observer->W*Observer->Tsample + Observer->K_teta0*(Observer->Ml_izm-Observer->Ml) + Observer->L1_w*(teta_dif);//� ��������
		Observer->W=Observer->W + Observer->K_teta1*(Observer->Ml_izm-Observer->Ml)+Observer->L2_w*(teta_dif);//� �������� �������������
		Observer->Ml=Observer->Ml+Observer->L3_w*teta_dif;
	}

	if(Observer->Teta_rad>toPI) 	Observer->Teta_rad-=toPI;
	if(Observer->Teta_rad<0.f)		Observer->Teta_rad+=toPI;
	
	if(Observer->refresh_ena==1)
	{
		refresh_koef(Observer);
		Observer->refresh_ena=0;
	}



}
//#pragma CODE_SECTION(refresh_koef,"ramfuncs");

inline void refresh_koef(Observer_struct *Observer)
{
	if(!Observer->fullMode_ena){
		Observer->L1_w=2.f*Observer->L0-2.f;
		Observer->L2_w=(-1.f+2.f*Observer->L0-Observer->L0*Observer->L0)/(Observer->Tsample);
	}else{
		Observer->L1_w=3*(Observer->L0-1);
		Observer->L2_w=(9*Observer->L0 - 3*Observer->L0*Observer->L0 - Observer->L0*Observer->L0*Observer->L0 - 5)/(2*Observer->Tsample);
		Observer->L3_w=Observer->J*(-3*Observer->L0 + 3*Observer->L0*Observer->L0 - Observer->L0*Observer->L0*Observer->L0 + 1)/(Observer->Tsample*Observer->Tsample );

		Observer->K_teta0 = Observer->Tsample * Observer->Tsample/(2*Observer->J);
		Observer->K_teta1 = Observer->Tsample / Observer->J;
	}
}
///------------------------------------------------------------------------

