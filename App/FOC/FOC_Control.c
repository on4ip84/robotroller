/*
 * FOC_Control.c
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: a.ermakov
 */

#include "PlatformMath.h"
#include "pid_reg.h"
#include "FOC_Control.h"
#include "FuncRestrict.h"
//const float32_t sinTable_f32[FAST_MATH_TABLE_SIZE + 1] __attribute__((section(".RamFunc")));
/*Constants*/
const float lag_correction=1.5f;
float dtime_volt = 0; /*Анпряжение мертвого времени*/
/*Struct definition*/
PIDREG3 Pid_Iq = PIDREG3_DEFAULTS;
PIDREG3 Pid_Id = PIDREG3_DEFAULTS;
PIDREG3 Pid_Idc = PIDREG3_DEFAULTS;
PIDREG3 Pid_FW =  PIDREG3_DEFAULTS;
PIDREG3 Pid_W = PIDREG3_DEFAULTS;
PARAM_st PARAMETERS = {0};
FuncRestricts_st RESTRICTS = {0};
DRIVE_st DRIVE={.drive_parameters = &PARAMETERS,
				.ppid_Iq = &Pid_Iq,
				.ppid_Id = &Pid_Id,
				.ppid_Idc = &Pid_Idc,
				.ppid_FW = &Pid_FW,
				.ppid_Speed = &Pid_W,
				.prestricts = &RESTRICTS};
union options_un actual_options = {0};
extern float Icog_compenstion;
/*Special mode used during HAND debug*/
#define HAND_MODE 0
float Teta_norm = 0;
volatile float Teta_shift=0;
enum ANGLE_REF
{
	ZERO_ANGLE=0,
	PLUS_2PIdiV3,
	MINUS_2PIdiV3
};
enum ANGLE_REF WORK_ANGLE=ZERO_ANGLE;
float Teta_ref[3]={0,2.094395f,-2.094395f};
float ElTheta_Hand=0;				/*Угол говорота в ручном режиме*/
float ElSpeed_Hand=0;				/*Скорость поворота в ручном режиме*/
float voltage_Vref_hand	=	0;		/*Задание понапряжению в вольтах в ручном режиме*/
float voltage_Vref_hand_d = 0;		/*Задание понапряжению в вольтах в ручном режиме*/
/****************************************************************************************************************/
/***********Attantion********************************************************************************************/

/* @detail For control Motor in DQ reference frame we  should use R S values scales from 3-Phase to 2 Phase model basically
 * 		it is 1.5 value. SO if we have R-phase in QD model we divide it to 1.5 for Real Phase resistance.
 * 		During PI current loop turning we should use DQ model values of L and R/
 * 		Kp it is Ldq*Bandwidth ( Rads????)
 * 		Ki it is L/R*Tsample for discrete current controller
 */

/**
  * @brief  This function transforms stator values a and b (which are
  *         directed along axes each displaced by 120 degrees) into values
  *         alpha and beta in a stationary qd reference frame.
  *                               alpha = a
  *                       beta = -(2*b+a)/sqrt(3)
  *
  *
  *
**/
static inline void Clark(DRIVE_st *drive)
{
	  //*                               alpha = a
	  //*                       beta = -(2*b+a)/sqrt(3)
	drive->currentClark.current_Ialpha = twoDIVthree*(drive->currentABC.current_Ia - 0.5f*drive->currentABC.current_Ib-0.5f*drive->currentABC.current_Ic);
	drive->currentClark.current_Ibeta = invSQRT_3*(drive->currentABC.current_Ib-drive->currentABC.current_Ic);
}
/**
  * @brief  This function transforms stator values alpha and beta, which
  *         belong to a stationary qd reference frame, to a rotor flux
  *         synchronous reference frame (properly oriented), so as q and d.
  *                   d= alpha *cos(theta)+ beta *sin(Theta)
  *                   q= -alpha *sin(Theta)+ beta *cos(Theta)
**/
static inline void Park(DRIVE_st *drive)
{
	static float sin_El=0,cos_El=0;
	platform_sincos(drive->ElAngle, &sin_El, &cos_El);
	drive->currentPark.current_Id = drive->currentClark.current_Ialpha*cos_El+drive->currentClark.current_Ibeta*sin_El;
	drive->currentPark.current_Iq = -drive->currentClark.current_Ialpha*sin_El+drive->currentClark.current_Ibeta*cos_El;
}
/**
 *  @brief invers Park transform function
 *  @detail	inverse transform
 *  	alpha = d*cos(theta)-beta*sin(theta)
 *  	beta = d*sin(theta)+beta*cos(theta)
 */
static inline void InversePark(DRIVE_st *drive)
{
	static float sin_El=0,cos_El=0;
    platform_sincos(drive->ElAngle,&sin_El,&cos_El);
    drive->voltage_Valpha_ref	=( drive->voltage_Vd_ref)*cos_El-( drive->voltage_Vq_ref)*sin_El;
    drive->voltage_Vbeta_ref	=( drive->voltage_Vd_ref)*sin_El+( drive->voltage_Vq_ref)*cos_El;
}
static inline void PowerCalc(DRIVE_st* drive)
{
	drive->power_Pdc = (drive->currentPark.current_Iq * (drive->voltage_Vq_ref - sign(drive->voltage_Vq_ref) * dtime_volt)+
					drive->currentPark.current_Id * (drive->voltage_Vd_ref - sign(drive->voltage_Vd_ref) * dtime_volt));
	drive->current_Idc = drive->power_Pdc/(drive->voltage_DC+0.001f);
}
static inline  void Rate_limiter(float *Rated_value, float Ref_value, float Up_rate, float Down_rate,float Tsample)
{
	const float Down = Tsample*Down_rate;
	const float Up = Tsample*Up_rate;
	if (Ref_value >(*Rated_value + Up))
	{
	*Rated_value += Up;
	}
	else if (Ref_value <(*Rated_value - Down))
	{
	*Rated_value -= Down;
	}
	else *Rated_value = Ref_value;
}
float K1_voltage = 0;
float K2_voltage = 0;
float K1_speed = 0;
float K2_speed = 0;

void filters_init(float voltage_tp, float elspeed_tp)
{
	K1_voltage = platform_exp(-DRIVE.Tsample_fast / voltage_tp);
	K2_voltage = 1.f - K1_voltage;

	K1_speed = platform_exp(-DRIVE.Tsample_slow /elspeed_tp);
	K2_speed = 1.f - K1_speed;
}
/**
 * .brief Zero cancellation function
 * \details Use IIR presentation of zero cancel block with equation y(k)=Ki*Ts/(Kp+Ki*Ts)*x(k)+kp/(kp+ki*Ts)*y(k-1)
 * \param pid PIDREG3_handle reference
 * \param ref Ref value to regulator
 * \return filtered value
 */
static float ZeroCancel(PIDREG3_handle pid, float ref)
{
	float out_val = 0;
	if((pid->Ki>0) || (pid->Kp > 0))
	out_val = pid->Ki / (pid->Kp + pid->Ki) * ref + pid->Kp / (pid->Kp + pid->Ki) * pid->Ref;
	return(out_val);
}

FAST_RAM void fast_loop_control(DRIVE_st* drive) ;
void fast_loop_control( DRIVE_st* drive)
{
	PIDREG3 *const FOC_Id = drive->ppid_Id;
	PIDREG3 *const FOC_Iq = drive->ppid_Iq;
	PIDREG3 *const FOC_Idc = drive->ppid_Idc;
	PIDREG3 *const FOC_FW = drive->ppid_FW;

	/*преобразование Кларк*/
	Clark(drive);
	/*преобразование Парка*/
    if(drive->ElAngle>=toPI)	drive->ElAngle	=	drive->ElAngle-toPI;
    if(drive->ElAngle<0.f)		drive->ElAngle	=	drive->ElAngle+toPI;
	Park(drive);
	/*Расчет параметров мощности по DC*/
	PowerCalc(drive);
	/*Сброс игтеграторов если не работаем*/
	if(!drive->STAT.stat.bit_ENABLED)
	{
		FOC_FW->dft_st_U		= 1u;				/*Сброс интгеральной составляющей*/
		FOC_Id->dft_st_U		= 1u;				/*Сброс интгеральной составляющей*/
		FOC_Iq->dft_st_U		= 1u;				/*Сброс интгеральной составляющей*/
		FOC_Idc->dft_st_U		= 1u;				/*Сброс интгеральной составляющей*/
	}

	drive->voltage_VrefFilt = K1_voltage * drive->voltage_VrefFilt + K2_voltage * drive->voltage_Vref;
	if(drive->options.options.bit_FluxWeak1) /*Алгоритм ослабления поля 1*/
	{
		  FOC_FW->Ref = drive->voltage_DC * FiledWeaking_coeff * VoltageLimitation_coeff;
		  FOC_FW->Fdb = drive->voltage_VrefFilt;
		  FOC_FW->OutMin = drive->drive_parameters->Idz_min;
		  FOC_FW->OutMax = drive->drive_parameters->Idz_max;
		  FOC_FW->calc(FOC_FW);
		  drive->current_Id_ref = FOC_FW->Out;
	}
	if(drive->options.options.bit_FluxWeak2) /*Алгоритм ослабления поля 2*/
	{
		FOC_FW->Ref = drive->voltage_DC * FiledWeaking_coeff * VoltageLimitation_coeff;
		FOC_FW->Fdb = drive->voltage_VrefFilt;
		float KI_BUFF = FOC_FW->Ki;
		FOC_FW->Ki = FOC_FW->Ki * 0.001f;
		FOC_FW->OutMin = -PI * 0.5f;
		FOC_FW->OutMax = 0;
		FOC_FW->calc(FOC_FW);
		FOC_FW->Ki = KI_BUFF;
		static float sin_fw = 0, cos_fw = 0;
		platform_sincos(FOC_FW->Out, &sin_fw, &cos_fw);
		drive->current_Id_ref = sin_fw * mod(drive->drive_parameters->Idz_min);
		if (drive->current_Id_ref < drive->drive_parameters->Idz_min) drive->current_Id_ref = drive->drive_parameters->Idz_min;
	}
	/*Вычисление задания по напряжению оси D*/
	FOC_Id->Ref = ZeroCancel(FOC_Id, drive->current_Id_ref);
	FOC_Id->Fdb = drive->currentPark.current_Id;
	FOC_Id->OutMax = drive->voltage_DC;
	FOC_Id->OutMin = -FOC_Id->OutMax;
	FOC_Id->calc(FOC_Id);
	/*Формирование здания по току*/

	/*Вычисление максимального тока оси q в функции тока Idc*/
	if (drive->STAT.stat.bit_DRIVE_MODE == MOTOR)
	{
		FOC_Idc->Ref = drive->drive_parameters->Ibattery_dischargeMax;
		FOC_Idc->OutMax = mod(drive->drive_parameters->Torque_current_max);
	}
	else
	{
		FOC_Idc->Ref = drive->drive_parameters->Ibattery_chargeMax;
		FOC_Idc->OutMax = mod(drive->drive_parameters->Brake_current_max);
	}
	FOC_Idc->Fdb = mod(drive->current_Idc);
	FOC_Idc->OutMax = mod(drive->torque_Mz_limited);
	FOC_Idc->OutMin = 0;
	FOC_Idc->calc(FOC_Idc);
	//drive->torque_Mz_avaible = (FOC_Idc->Out);
	/*Ограничение по доступному току*/
	if (drive->torque_Mz_limited > (FOC_Idc->Out)) drive->current_Iq_ref = (FOC_Idc->Out);
	else if (drive->torque_Mz_limited < (-(FOC_Idc->Out)))  drive->current_Iq_ref = (-(FOC_Idc->Out));
	else drive->current_Iq_ref = drive->torque_Mz_limited;
	/*Вычисление задания по напряжению оси Q*/
	FOC_Iq->Ref = ZeroCancel(FOC_Iq, drive->current_Iq_ref);
	FOC_Iq->Fdb = drive->currentPark.current_Iq;
	FOC_Iq->OutMax = drive->voltage_DC;
	FOC_Iq->OutMin = -FOC_Iq->OutMax;
	FOC_Iq->calc(FOC_Iq);

	/*Обработка алгоритма feedforward*/
	if(drive->options.options.bit_Decouple){
	  drive->voltage_Vd_decoupl=drive->motor_data.Rs*FOC_Id->Fdb - drive->ElSpeed*drive->motor_data.Lq  *FOC_Iq->Fdb;
	  drive->voltage_Vq_decoupl=drive->motor_data.Rs*FOC_Iq->Fdb + drive->ElSpeed*drive->motor_data.Ld  *FOC_Id->Fdb + drive->ElSpeed*drive->motor_data.Emf;
	}
	else{
		drive->voltage_Vd_decoupl =0;
		drive->voltage_Vq_decoupl =0;
	}
#if 1
	drive->voltage_Vq_ref = FOC_Iq->Out + drive->voltage_Vq_decoupl;
	drive->voltage_Vd_ref = FOC_Id->Out + drive->voltage_Vd_decoupl;
#else
	drive->voltage_Vq_ref = voltage_Vref_hand + drive->torque_Mz;
	drive->voltage_Vd_ref = voltage_Vref_hand_d;
#endif
#if 1
	/*Обработка лимитирования выходного напряжения*/
	drive->voltage_Vref = platform_sqrt(drive->voltage_Vd_ref * drive->voltage_Vd_ref + drive->voltage_Vq_ref * drive->voltage_Vq_ref);
	float max_amp = (drive->voltage_DC-dtime_volt) * VoltageNormalisation_coeff;
	float klim = max_amp / (drive->voltage_Vref + 0.1f);
	if (klim < 1.0f){
		drive->voltage_Vq_ref = drive->voltage_Vq_ref * klim;
		drive->voltage_Vd_ref = drive->voltage_Vd_ref * klim;
	}
#else
	drive->voltage_Vref = platform_sqrt(drive->voltage_Vd_ref * drive->voltage_Vd_ref + drive->voltage_Vq_ref * drive->voltage_Vq_ref);
	float max_vd = (drive->voltage_DC - dtime_volt) * VoltageNormalisation_coeff * 0.9f;
	if (drive->voltage_Vd_ref > max_vd) drive->voltage_Vd_ref = max_vd;
	if (drive->voltage_Vd_ref <-max_vd) drive->voltage_Vd_ref = -max_vd;
	float max_vq = platform_sqrt(drive->voltage_DC * drive->voltage_DC * VoltageNormalisation_coeff * VoltageNormalisation_coeff - drive->voltage_Vd_ref * drive->voltage_Vd_ref);
	if (drive->voltage_Vq_ref > max_vd) drive->voltage_Vq_ref = max_vq;
	if (drive->voltage_Vq_ref < -max_vd) drive->voltage_Vq_ref = -max_vq;
#endif

#if !HAND_MODE
	/*Обратное преобразование Парка */
    if(mod(drive->ElSpeed) > 50.f) drive->ElAngle = drive->ElAngle + drive->Tsample_fast* drive->ElSpeed * lag_correction;
    if(drive->ElAngle>=toPI)	drive->ElAngle	=	drive->ElAngle-toPI;
    if(drive->ElAngle<0.f)		drive->ElAngle	=	drive->ElAngle+toPI;
    InversePark(drive);
#else
	  static float cos_teta_el=0,sin_teta_el=0;
	  ElTheta_Hand=ElTheta_Hand+drive->Tsample_fast*ElSpeed_Hand;
	  if(ElTheta_Hand>=toPI) ElTheta_Hand=ElTheta_Hand-toPI;
	  if(ElTheta_Hand<0) ElTheta_Hand=ElTheta_Hand+toPI;
	  platform_sincos((ElTheta_Hand+Teta_ref[WORK_ANGLE]),&sin_teta_el,&cos_teta_el);
	  drive->voltage_Valpha_ref	=voltage_Vref_hand*cos_teta_el;
	  drive->voltage_Vbeta_ref	=voltage_Vref_hand*sin_teta_el;
#endif
}

#define THRESHOLD 0.0000005f
_inline float Speed_Control(DRIVE_st* drive)
{
 	PIDREG3 *const FOC_Speed = drive->ppid_Speed;
	/*Ограничение задания по скорости*/
	if(drive->speed_Ref>drive->drive_parameters->Max_speed) drive->speed_Ref = drive->drive_parameters->Max_speed;
	if(drive->speed_Ref<drive->drive_parameters->Min_speed) drive->speed_Ref = drive->drive_parameters->Min_speed;
	/*Сохраненеие знака скорости*/
	int sign_Fz=sign(drive->speed_Fdb);

	float slope_up = drive->Tsample_slow*drive->drive_parameters->Speed_ramp_Up;
	float slope_down = drive->Tsample_slow*drive->drive_parameters->Speed_ramp_Down;
	if(sign_Fz>0)
	{
	if(drive->speed_Ref > drive->speed_Ref_rated+slope_up)
	{
		drive->speed_Ref_rated+=slope_up;
	}
	else if (drive->speed_Ref < drive->speed_Ref_rated-slope_down)
	{
		drive->speed_Ref_rated-=slope_down;
	}
	else drive->speed_Ref_rated = drive->speed_Ref;
	}else
	{
	if(drive->speed_Ref > drive->speed_Ref_rated+slope_down)
	{
		drive->speed_Ref_rated+=slope_down;
	}
	else if (drive->speed_Ref <drive->speed_Ref_rated-slope_up)
	{
		drive->speed_Ref_rated-=slope_up;
	}
	else drive->speed_Ref_rated = drive->speed_Ref;
	}
	/*Сброс интегральной составляющей скорости*/
	if(!drive->STAT.stat.bit_ENABLED)
	{
		FOC_Speed->dft_st_U = 1;
	}
	FOC_Speed->Ref = drive->speed_Ref_rated;
	FOC_Speed->Fdb = drive->speed_Fdb;
	FOC_Speed->OutMax = drive->drive_parameters->Torque_current_max;
	FOC_Speed->OutMin = -drive->drive_parameters->Brake_current_max;
	FOC_Speed->calc(FOC_Speed);
	if(mod(FOC_Speed->Out)<THRESHOLD) FOC_Speed->Out = 0;
	return (FOC_Speed->Out);
}
float test_t = 0;
FAST_RAM void slow_loop_control(DRIVE_st* drive) ;
void slow_loop_control(DRIVE_st* drive)
{
	register FuncRestricts_st *const RESTRICTS = drive->prestricts;
	register float Mz_buffer = 0;
	register float Work_ramp_Up=0;
	register float Work_ramp_Down=0;
	/*Машина состояний режимов*/
	drive->STAT.stat.bit_SPEED_MODE = 0;
	drive->STAT.stat.bit_REVERSE_MODE = 0;
	switch (drive->CMD.cmd.bit_CTRL_MODE)
	{
		case SPEED:
		{
			drive->torque_Mz = Speed_Control(drive);
			Mz_buffer = drive->torque_Mz;
			Work_ramp_Up = 10000;
			Work_ramp_Down = 10000;
			drive->STAT.stat.bit_SPEED_MODE = 1;
			break;
		}
		case TORQUE:
		{
			/*teoretical FULL torque region speed*/
			float	a = drive->motor_data.Emf * drive->motor_data.Emf + drive->drive_parameters->Torque_current_max * drive->drive_parameters->Torque_current_max * drive->motor_data.Ld * 1.5f * drive->motor_data.Ld * 1.5f;
			float	b = 2.f * drive->drive_parameters->Torque_current_max * drive->motor_data.Rs * 1.5f * drive->motor_data.Emf;
			float	c = drive->drive_parameters->Torque_current_max * drive->drive_parameters->Torque_current_max * drive->motor_data.Rs * 1.5f * drive->motor_data.Rs * 1.5f - drive->voltage_DC * drive->voltage_DC * VoltageNormalisation_coeff * VoltageNormalisation_coeff;
			float	speed_to_TRQlimit = (-b + platform_sqrt(b * b - 4.f * a * c)) / (2.f * a);
			float	torq2power_coef = speed_to_TRQlimit * 0.9f / (mod(drive->ElSpeed) + 0.1f);
			if (torq2power_coef < 1.f) drive->torque_Mz_avaible = drive->drive_parameters->Torque_current_max * torq2power_coef;
			else
			{
				drive->torque_Mz_avaible = drive->drive_parameters->Torque_current_max;
			}
			register float  abs_Mz = mod(drive->torque_Mz_avaible);
			if (drive->torque_Mz > abs_Mz) Mz_buffer = abs_Mz;
			else if (drive->torque_Mz < -abs_Mz) Mz_buffer = -abs_Mz;
				else Mz_buffer = drive->torque_Mz; //Buffer variable
			Work_ramp_Up = drive->drive_parameters->Torque_ramp_Up;
			Work_ramp_Down = drive->drive_parameters->Torque_ramp_Down;
			break;
		}
		case REGEN_BRAKING:
		{
			Mz_buffer = -drive->torque_Mz * sat_sign(drive->ElSpeed , 0.005f);
			Work_ramp_Up = drive->drive_parameters->Brake_ramp_Up;
			Work_ramp_Down = drive->drive_parameters->Brake_ramp_Down;
			break;
		}
		case SWITCH_BRAKING:
		{

			Mz_buffer = -drive->torque_Mz * sat_sign(drive->ElSpeed, 0.005f);
			Work_ramp_Up = drive->drive_parameters->Brake_ramp_Up;
			Work_ramp_Down = drive->drive_parameters->Brake_ramp_Down;
			break;
		}
		case POSTION:
		{

			break;
		}
	}//end if switch
	/*Calculate motor torque*/
	drive->torque_calc = 1.5f * 0.5774f * drive->currentPark.current_Iq * drive->motor_data.Emf*drive->motor_data.P;
	/*Calculate motor power*/
	drive->power_Mech = drive->torque_calc * drive->ElSpeed / (drive->motor_data.P + 0.01f);
	/*Calculate Flux Shift*/
	drive->flux_shift_calc = drive->flux_shift_calc * K1_speed+ platform_atan2(mod(drive->voltage_Vd_ref),mod(drive->voltage_Vq_ref)) * K2_speed;
	if(mod(drive->ElSpeed)>50.f)
	{
		drive->flux_calc = K1_speed * drive->flux_calc + (drive->voltage_Vq_ref/drive->ElSpeed) * K2_speed;
	}
	/*Rate limiter*/
	Rate_limiter(&drive->torque_Mz_rated, Mz_buffer, Work_ramp_Up, Work_ramp_Down, drive->Tsample_slow);
	/*Индикация режиа работы привода по квадранту*/
	if(sign(drive->ElSpeed * drive->torque_Mz_rated) >= 0) drive->STAT.stat.bit_DRIVE_MODE = MOTOR;
	else drive->STAT.stat.bit_DRIVE_MODE = GENERATOR;
	/*Блок функциональных ограничений*/
	float m_max = 0;
	m_max = drive->torque_Mz_rated 	*	ProcessRestricts(drive->ElSpeed,RESTRICTS,PositiveSpeed_constraint);
	m_max = m_max *	ProcessRestricts(drive->ElSpeed,RESTRICTS,NegativeSpeed_constraint);
	if(drive->STAT.stat.bit_DRIVE_MODE == GENERATOR)
			m_max = m_max *	ProcessRestricts(drive->voltage_DC,RESTRICTS,HighVolt_constraint);
	else	
			m_max = m_max *	ProcessRestricts(drive->voltage_DC,RESTRICTS,LowVolt_constraint);

	m_max = m_max *	ProcessRestricts(test_t,RESTRICTS,HighTemperature1_constraint);
	m_max = m_max *	ProcessRestricts(test_t,RESTRICTS,LowTemparature1_constraint);
	drive->torque_Mz_limited = m_max;
}
