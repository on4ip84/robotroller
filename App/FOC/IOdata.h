/*
 * IOdata.h
 *
 *  Created on: 3 июн. 2021 г.
 *      Author: a.ermakov
 */

#ifndef APPLICATION_USER_IODATA_H_
#define APPLICATION_USER_IODATA_H_
#include "stdint-gcc.h"
typedef struct
{
	float ADC_A_Ku;
	float ADC_A_shift;
	float ADC_B_Ku;
	float ADC_B_shift;
	float ADC_C_Ku;
	float ADC_C_shift;
	float ADC_Udc_Ku;
	float ADC_Udc_shift;
}IO_settings_st;
typedef struct
{
	IO_settings_st *IO_coefs;
	int16_t ADC_A;
	float current_A;
	int16_t ADC_B;
	float current_B;
	int16_t ADC_C;
	float current_C;
	uint16_t ENC_CNT_AB;
	uint16_t ENC_CNT_SPI;
	uint16_t ENC_CNT_PWM;
	uint16_t ADC_Udc;
	float voltage_DC;
	uint16_t ADC_Tpower;
	float temparature_power;
	uint16_t ADC_VrefInt;
	float voltage_VrefInt;
}IO_st;
extern IO_st IO;
extern IO_settings_st IO_settings_ext;
/*!
 * @
 */
void update_IO_ADC(IO_st* io,uint16_t *ADC_data,const uint16_t mode);
void update_IO_Sensor(IO_st* io,uint16_t pos_cnt);
void set_IOSettings(IO_st *io,IO_settings_st* settings);
#endif /* APPLICATION_USER_IODATA_H_ */
