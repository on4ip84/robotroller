/*
 * Filters.h
 *
 *  Created on: 16 авг. 2021 г.
 *      Author: v.timokhin
 */

#ifndef APP_FOC_FILTERS_H_
#define APP_FOC_FILTERS_H_
#include "PlatformMath.h"
#include "stdint.h"
#include "math.h"
typedef struct
{
	float k1;
	float k2;
	float filtVal;
}lowPass_st;
extern lowPass_st GyroLowPass;
void initFilter(lowPass_st *filter,float Tsample,float Tp);
void filterProc(lowPass_st* filter,float rawVal);
#endif /* APP_FOC_FILTERS_H_ */
