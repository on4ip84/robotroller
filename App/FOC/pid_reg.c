/*
 * pid_reg.c
 *
 *  Created on: 4 июн. 2021 г.
 *      Author: a.ermakov
 */
#include "pid_reg.h"
void pid_reg3_calc(PIDREG3 *v)
{

	// Compute the error
    v->Err = v->Ref - v->Fdb;

    // Compute the proportional output
    v->Up = v->Kp*v->Err;

    // Compute the integral output
    //v->Ui = v->Ui + v->Ki*v->Up + v->Kc*v->SatErr;
    v->Ui = v->Ui + v->Ki*v->Err + v->Kc*v->SatErr;
    if(v->Ui > v->OutMax ) v->Ui = v->OutMax;
    if(v->Ui < v->OutMin ) v->Ui = v->OutMin;
    // Compute the derivative output
    //v->Ud = v->Kd*(v->Up - v->Up1);
    float Ud_tmp = v->Kd*(v->Up - v->Up1);
    // Filter for Ud part
    v->Ud = v->Ud+v->Tfilt_d*(Ud_tmp-v->Ud);
    // Compute the pre-saturated output
    v->OutPreSat = v->Up + v->Ui + v->Ud;

    // Saturate the output
    if (v->OutPreSat > v->OutMax)
      v->Out =  v->OutMax;
    else if (v->OutPreSat < v->OutMin)
      v->Out =  v->OutMin;
    else
      v->Out = v->OutPreSat;
    if(v->dft_st_U==1)
	{
		//v->Out=0;
		v->Ui=0;
	}
    v->dft_st_U = 0;
    // Compute the saturate difference
    v->SatErr = v->Out - v->OutPreSat;

    // Update the previous proportional output
    v->Up1 = v->Up;
    //v->Up1 = v->Err;

}
void pid_reg3_calc_position(PIDREG3 *v)
{
    // Compute the proportional output
    v->Up = v->Kp*v->Err;
    // Compute the integral output
    //v->Ui = v->Ui + v->Ki*v->Up + v->Kc*v->SatErr;
    v->Ui = v->Ui + v->Ki*v->Err + v->Kc*v->SatErr;
    if(v->Ui > v->OutMax ) v->Ui = v->OutMax;
    if(v->Ui < v->OutMin ) v->Ui = v->OutMin;
    // Compute the derivative output
    //v->Ud = v->Kd*(v->Up - v->Up1);
    float Ud_tmp = v->Kd*(v->Up - v->Up1);
    // Filter for Ud part
    v->Ud = v->Ud+v->Tfilt_d*(Ud_tmp-v->Ud);
    // Compute the pre-saturated output
    v->OutPreSat = v->Up + v->Ui + v->Ud;

    // Saturate the output
    if (v->OutPreSat > v->OutMax)
      v->Out =  v->OutMax;
    else if (v->OutPreSat < v->OutMin)
      v->Out =  v->OutMin;
    else
      v->Out = v->OutPreSat;
    if(v->dft_st_U==1)
	{
		//v->Out=0;
		v->Ui=0;
	}
    v->dft_st_U = 0;
    // Compute the saturate difference
    v->SatErr = v->Out - v->OutPreSat;

    // Update the previous proportional output
    v->Up1 = v->Up;
    //v->Up1 = v->Err;

}
void pid_reg3_calc_clamp(PIDREG3 *v)
{

	// Compute the error
    v->Err = v->Ref - v->Fdb;

    // Compute the proportional output
    v->Up = v->Kp*v->Err;

    // Compute the integral output
    //v->Ui = v->Ui + v->Ki*v->Up + v->Kc*v->SatErr;
    v->Ui = v->Ui + v->Ki*v->Err *v->Up1;
    // Compute the pre-saturated output
    v->OutPreSat = v->Up + v->Ui;
    v->Up1 = 1.f;//No saturation
    // Saturate the output
    if (v->OutPreSat > v->OutMax)
    {
      v->Out =  v->OutMax;
      v->Up1 = 0;
    }
    else if (v->OutPreSat < v->OutMin)
    {
      v->Out =  v->OutMin;
      v->Up1 = 0;
    }
    else
      v->Out = v->OutPreSat;
    if(v->dft_st_U==1)
	{
		//v->Out=0;
		v->Ui=0;
	}
    v->dft_st_U = 0;
    // Compute the saturate difference
    v->SatErr = v->Out - v->OutPreSat;


}

void pid_reg3_calc_ErrLim(PIDREG3 *v)
{
	// Compute the error
    v->Err = v->Ref - v->Fdb;
    // Compute the proportional output
    v->Up = v->Kp*v->Err;
    // Compute the pre-saturated output
    v->OutPreSat = v->Up + v->Ui;
    // Saturate the output
    v->Up1 = 0;
    if (v->OutPreSat > v->OutMax)
    {
      v->Out =  v->OutMax;
      v->Up1 = v->OutMax - v->OutPreSat  ;
    }
    else if (v->OutPreSat < v->OutMin)
    {
      v->Out =  v->OutMin;
      v->Up1 = v->OutMin - v->OutPreSat  ;
    }else v->Out = v->OutPreSat;
    //Intergral err limit
    v->Ui=v->Ui+v->Ki*(v->Err + v->Up1/v->Kp);

    if(v->dft_st_U==1)
	{
		//v->Out=0;
		v->Ui=0;
	}
    v->dft_st_U = 0;
}

void set_PI_coefs(PIDREG3 *v,float Kp,float Ki,float Kc)
{
	v->Kp = Kp;
	v->Ki = Ki;
	v->Kc = Kc;
}
