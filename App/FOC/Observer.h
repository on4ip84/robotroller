#ifndef INCLUDE_OBSERVER_H_
#define INCLUDE_OBSERVER_H_
#include "math.h"

typedef struct
{
		float Teta_diskr;// ������������ � �� ��������
		float POS_rad;// ������� ���� � �������� ��
		float last_CNT;// ������� �������� �������� ���������
		float POS_dobavka_sum;// ��������� ������� �� ���� �� ����� 1 ��������
		unsigned int fullMode_ena;// ���� ����� �������
		unsigned int refresh_ena;//��� ���������� ����
		float teta_correction;//������� �������� ���� � �� ��������
		float Teta_rad;// ������������ ��� ������
		float Teta_deg;// ���� � ���������� ���������� ��������
		float W; //������������ �������� ������
		float Interpolate_W_Start;// �������� � ������� ����������� ������� � ���� �� teta+dt*w
		float K_teta0; //���� ��� ��������� ��������
		float K_teta1; // ���� ��� ���������
		float CNT;//������� ���������
		float Ml_izm;
		float Ml;
		unsigned int Time_CNT;// ������� ������� ����� ����������
		float J;//������ ������� �������
		float L0;//���� ����������
		float Tsample;//���� ����� �������
		float L1_w;// ���� ��� �����������
		float L2_w;
		float L3_w;

} Observer_struct;
extern Observer_struct Observer;
extern Observer_struct ObserverM;
void refresh_koef(Observer_struct *Observer);
void init_observer(Observer_struct *Observer);
void speed_observer(Observer_struct *Observer);
#endif /*INCLUDE_OBSERVER_H_*/
