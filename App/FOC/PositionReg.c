/*
 * PositionReg.c
 *
 *  Created on: 9 июн. 2021 г.
 *      Author: a.ermakov
 */
#include "PositionReg.h"
#include "SystemAPI.h"

enum REG_status  Reg_state=DISABLED;
PIDREG3 Pid_Position = PIDREG3_DEFAULTS;
PIDREG3 Pid_Speed	=	PIDREG3_DEFAULTS;
float current_reference = 0;
#define DIRECT_POSITION 1
#define GRAVITY_COMPENSTATION 0
/**
 * .brief Zero cancellation function
 * \details Use IIR presentation of zero cancel block with equation y(k)=Ki*Ts/(Kp+Ki*Ts)*x(k)+kp/(kp+ki*Ts)*y(k-1)
 * \param pid PIDREG3_handle reference
 * \param ref Ref value to regulator
 * \return filtered value
 */
static float ZeroCancel(PIDREG3_handle pid, float ref)
{
	float out_val = 0;
	if((pid->Ki>0) || (pid->Kp > 0))
	out_val = pid->Ki / (pid->Kp + pid->Ki) * ref + pid->Kp / (pid->Kp + pid->Ki) * pid->Ref;
	return(out_val);
}

void Position_reg_init(void)
{

	Pid_Speed.calc = &pid_reg3_calc;
	Pid_Speed.Kp = 0.61;
	Pid_Speed.Ki = 0.0003233;
	Pid_Speed.Tfilt_d = 0.1f;
	Pid_Speed.Kd = 0;
	Pid_Speed.OutMax = 1;
	Pid_Speed.OutMin = -1;

	Pid_Position.calc = &pid_reg3_calc_position;
	Pid_Position.Kp = 2.56;
	Pid_Position.Ki = 0.009233;
	Pid_Position.Tfilt_d = 0.01f;
	Pid_Position.Kd = 540;
	Pid_Position.OutMax = DEF_MAX_CURRENT;
	Pid_Position.OutMin = -DEF_MAX_CURRENT;

}
//#define radToImp 4000/toPI
#define radToImp toPI/16383
extern int16_t raw_angular_rate_X;
extern int16_t raw_angular_rate_Y;
extern int16_t raw_angular_rate_Z;
float angular_speed_coef = 0;
int16_t *p_angular_speed[2]={&raw_angular_rate_X,&raw_angular_rate_Y};
enum {
	X=0,
	Y
}axie ={X};
FAST_RAM void Position_reg_process(float ref,float fdb) ;
void Position_reg_process(float ref,float fdb)
{
	Pid_Position.Ref = ZeroCancel(&Pid_Position,ref);//ref;
	Pid_Position.Fdb = fdb;
	Pid_Position.Err = Pid_Position.Ref - Pid_Position.Fdb;
	/*исключение сброса на 2 пи*/
	Pid_Position.Err = rollPI(Pid_Position.Err);
	Pid_Position.Err = rollPI(Pid_Position.Err);
	/*Расчет пи регулятора*/
	if(Reg_state != ENABLED) Pid_Position.dft_st_U = 1u;
	if(mod(Pid_Position.Err) < (radToImp*2.f) ) Pid_Position.Err = 0;
	/*Gain control */
	Pid_Position.calc(&Pid_Position);

#if GRAVITY_COMPENSTATION
	current_reference = Pid_Position.Out + platform_sin(fdb)*0.345f+*p_angular_speed[axie]*angular_speed_coef;
#else
	current_reference = Pid_Position.Out;// + platform_sin(fdb)*0.345f+*p_angular_speed[axie]*angular_speed_coef;
#endif

}
void SetSpeedReg_IC(float ui)
{
	Pid_Speed.Ui = ui;
}
void Speed_reg_process(float ref , float fdb)
{
	Pid_Speed.Ref =ref;
	Pid_Speed.Fdb =fdb;
	Pid_Speed.calc(&Pid_Speed);
	current_reference = Pid_Speed.Out;
}
void Position_reg_processV2(float ref,float fdb,float speed_fdb)
{

	static float position_error;
	position_error = ref - fdb;
	/*исключение сброса на 2 пи*/
	position_error = rollPI(position_error);
	position_error = rollPI(position_error);
	if(mod(position_error) < radToImp ) position_error = 0;
	Pid_Position.Ui+=Pid_Position.Ki*position_error;
	/*Расчет пи регулятора*/
	if(Reg_state != ENABLED) Pid_Position.Ui = 0;
	Pid_Position.Up =  Pid_Position.Kp*fdb;
	Pid_Position.Ud =  -Pid_Position.Kd*speed_fdb;

#if GRAVITY_COMPENSTATION
	current_reference = Pid_Position.Out + platform_sin(fdb)*0.345f+*p_angular_speed[axie]*angular_speed_coef;
#else
	current_reference = Pid_Position.Ui-Pid_Position.Kp*fdb-Pid_Position.Kd*speed_fdb;// + platform_sin(fdb)*0.345f+*p_angular_speed[axie]*angular_speed_coef;
	if(current_reference > Pid_Position.OutMax)
		{
			current_reference = Pid_Position.OutMax;
			//Pid_Position.Ui = current_reference ;
		}
	if(current_reference < Pid_Position.OutMin){
		current_reference = Pid_Position.OutMin;
		//Pid_Position.Ui = current_reference ;
	}
#endif

}
float get_reference(void)
{
	return current_reference;
}

enum REG_status get_pos_reg_state(void)
{
	return Reg_state;
}
void set_pos_reg_state(enum REG_status state)
{
	Reg_state = state;
}
