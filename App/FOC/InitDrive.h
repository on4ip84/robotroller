
/*
 * InitDrive.h
 *
 *  Created on: 9 июн. 2021 г.
 *      Author: a.ermakov
 */

#ifndef APPLICATION_USER_INITDRIVE_H_
#define APPLICATION_USER_INITDRIVE_H_
#include "PlatformMath.h"
#include "IOdata.h"
#include "Observer.h"
#include "FOC_Control.h"
#include "pid_reg.h"
#include "FuncRestrict.h"
#include "PWM_algorithms.h"
#include "Filters.h"
#include "PositionReg.h"
/*SYStem object defination for Drive*/
typedef struct
{
	IO_st 			*IO;
	DRIVE_st 		*DRIVE;
	Observer_struct *OBSERVER_El;
	Observer_struct *OBSERVER_M;
	PWM_struct		*PWM;
	uint16_t 		CurrentRegBand;
}SYSTEMobj_st;
/*Make System Objects external*/
extern SYSTEMobj_st SYSTEMobj_1;
extern uint32_t CPU_ID;
void DriveInit(const float, const float);
#endif /* APPLICATION_USER_INITDRIVE_H_ */
