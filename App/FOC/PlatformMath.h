/*
 * PlatformMath.h
 *
 *  Created on: 7 июн. 2021 г.
 *      Author: a.ermakov
 */

#ifndef APPLICATION_USER_PLATFORMMATH_H_
#define APPLICATION_USER_PLATFORMMATH_H_
#include "arm_math.h"
#include "arm_common_tables.h"
#define _inline  static inline
#define PLATFORM_STM32 0
#define toPI 6.28318530f
#ifndef PI
#define PI 3.141592f
#endif
#define rad2Deg 57.295779513f
#define	mod(x)	( ((x)>0)? (x): (-(x)) )
#define	sign(x)	( ((x)>=0)? (1): (-(1)) )
#define min(x,y) ( ((x)>(y))? (y): (x) )
#define invSQRT_3 0.5773502691896f
#define twoDIVthree 2.f/3.f
#define rad2Deg 57.295779513f
/*Using FAST RAM*/
#define FAST_RAM __attribute__((section(".ccmram")))
//#define FAST_RAM
/**
 * Fast atan2
 *
 * See http://www.dspguru.com/dsp/tricks/fixed-point-atan2-with-self-normalization
 *
 * @param y
 * y
 *
 * @param x
 * x
 *
 * @return
 * The angle in radians
 */
#define PI_DIV_4 toPI/8.f
/*
static float utils_fast_atan2(float y, float x) {
	float abs_y = fabsf(y) + 1e-20f; // kludge to prevent 0/0 condition
	float angle;

	if (x >= 0.f) {
		float r = (x - abs_y) / (x + abs_y);
		float rsq = r * r;
		angle = ((0.1963f * rsq) - 0.9817f) * r + (PI_DIV_4);
	} else {
		float r = (x + abs_y) / (abs_y - x);
		float rsq = r * r;
		angle = ((0.1963f * rsq) - 0.9817f) * r + (PI_DIV_4);
	}

	if (y < 0.f) {
		return(-angle);
	} else {
		return(angle);
	}
}
*/
_inline float platform_sqrt(float ref)
{
	float out=0.f;
	if(ref > 0)	out = sqrtf(ref);
	return (out);
}
_inline float platform_isqrt(float val)
{
	if(val>0) return (1/platform_sqrt(val));
	return 0;
}
FAST_RAM void arm_sin_cos_f32(
        float32_t theta,
        float32_t * pSinVal,
        float32_t * pCosVal);
_inline float platform_sin(float theta)
{
	return(arm_sin_f32(theta));
}
_inline void platform_sincos(float theta,float *pSinVal,float *pCosVal)
{
	arm_sin_cos_f32((theta*rad2Deg), pSinVal, pCosVal);
}
_inline float platform_atan2(float X,float Y )
{
	return atan2f(X,Y);
	//return utils_fast_atan2(X,Y);
}
_inline float platform_exp(float x)
{
	return expf(x);
}
/*!
 * \brief Функция Знака в виде усилитель с насыщением.
 * @param I Текущее значение
 * @param k Коэффициент нечувствительности
 * @return
 */
_inline float  sat_sign(float I, float k)
{
	float out;
	out = k * I;
	if (out >= 1) return (1.);
	else if (out <= -1) return (-1.);
	return (0.f);
}

#endif /* APPLICATION_USER_PLATFORMMATH_H_ */
