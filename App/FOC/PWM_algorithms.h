/*
 * PWM_algorithms.h
 *
 *  Created on: 07 ���. 2017 �.
 *      Author: Andrey
 */

#ifndef FOC_CODE_PWM_ALGORITHMS_H_
#define FOC_CODE_PWM_ALGORITHMS_H_

/*!
 * \brief Struct describe pwm module and used in all PWM algorithms
 */
typedef struct
{

	float			Trpd_pwm;			///< �������� �������� ��� Tpwm=1/Fpc*2
	float			Ualfa;				///< ������� �� ��� �����
	float			Ubeta;				///< ������� �� ���� ����
	float			Uo;					///< ������� �� ������� �����
	float			Ta;					///< �������� ������� ��� ����������� ��� ���� �
	float			Tb;					///< �������� ������� ��� ����������� ��� ���� �
	float			Tc;					///< �������� ������� ��� ����������� ��� ���� �
	unsigned int 	sector;				///< ������� ������ ������ ��� ������������

} PWM_struct;
#define PWM_DEFAULTS {1,0,0,0,0.5,0.5,0.5,0}
extern PWM_struct MY_PWM;
unsigned int PWM_proc(PWM_struct * myPWM);
unsigned int Sine_PWM_proc(PWM_struct * myPWM);
unsigned int PWM_SIX_step(PWM_struct * myPWM);
void PWM_deadTime(float *Currents,float Kdt,PWM_struct * myPWM);
#endif /* FOC_CODE_PWM_ALGORITHMS_H_ */
