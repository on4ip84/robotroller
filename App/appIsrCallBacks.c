/*
 * appIsrCallBacks.c
 *
 *  Created on: 18 авг. 2023 г.
 *      Author: on4ip
 */

#include "main.h"
#include "ControlFuncs.h"

uint32_t adcTick = 0;
uint16_t adc1Data = 0;
uint16_t adc2Data = 0;

float ADC_ISR_time = 0; /*ISR load time*/
static uint16_t ADC_array[16];
void adcIsrCallBack(void) {

	static uint32_t start_ADC_isr = 0;
	static uint32_t stop_ADC_isr = 0;
	LL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	/*Time test of ADC isr*/
	start_ADC_isr = SysTick->VAL;
	adcTick++;
	/*Reading and processing ADC data*/
	uint16_t error_code = 0;
	if(GET_ADC_data(SYSTEMobj_1.DRIVE->drive_ID,ADC_array,&error_code))
	{
		SYSTEMobj_1.DRIVE->ERRORS.errors.bit_FREE1 = 1;
	}
	update_IO_ADC(SYSTEMobj_1.IO, ADC_array,SYSTEMobj_1.DRIVE->STAT.stat.bit_ENABLED);
	/*Calling of Application control function with Fast update rate*/
	//FAST_loop(&SYSTEMobj_1);
	stop_ADC_isr = SysTick->VAL;
	ADC_ISR_time = ISR_TIME_CALC(start_ADC_isr, stop_ADC_isr);
	LL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	LL_ADC_ClearFlag_JEOS(ADC1);

}
uint32_t tim1msTick = 0;
float TIM1ms_ISR_time = 0; /*ISR load time*/
void tim1msCallBack(void) {
	tim1msTick++;
	/*Colling of Application control function with Slow update rate*/
	SLOW_loop(&SYSTEMobj_1);
	//LL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
	LL_TIM_ClearFlag_UPDATE(TIM2);
}
uint32_t timPwmTick = 0;
void timPwmCallBack(void) {
	timPwmTick++;
	//LL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
	LL_TIM_ClearFlag_CC4(TIM1);
}
